# Igloo Engineering Tech Challenge
## ✏️ Summary
Your task is to build a simple 'income tax calculator' application. The application should take an input of a user's gross salary, and apply the following tax bands in order to provide them with a split of how much they will be taxed on their salary, outputting their net salary as a result.

![Income Tax Bands](tax-bands.png)

If you want to have a stab at building some additional features on top of this, we've included some [example extension tasks](#markdown-header-extensions) at the bottom of this document.

It is up to you whether you wish to build this as a front-end application or as a back-end, API-based application. Please see below some guidelines for both the front-end and back-end approach.

---

## Option 1: Front-end Application
### 💻 Technologies
The choice of framework to build this application is up to you. We'd suggest the use of either [Vue.js](https://vuejs.org/), [Angular](https://angular.io/) or [React](https://reactjs.org/) and if applicable you should make use of a state management library such as [Vuex](https://vuex.vuejs.org/), [Redux](https://redux.js.org/), [NgRx](https://ngrx.io/) or similar - however this is not required.

### 🎨 Design
Make use of any design framework you feel comfortable with - this could be something like [Bootstrap](https://getbootstrap.com/) or [Material Design](https://material.io/design/), or alternatively feel free to adapt our own branding from [our website](https://igloo.energy) or design something of your own.

---

## Option 2: Back-end Application
### 💻 Technologies
The choice of technologies to build this application is up to you. We'd suggest the use of either [Node.js](https://nodejs.org/) + [Express](https://expressjs.com/), [Python](https://python.org/) + [Flask](https://flask.palletsprojects.com/) or [Golang](https://golang.org/) + [Gin](https://github.com/gin-gonic/gin). Alternative technologies are accepted, as long as these are easily runnable, preferably through an included Dockerfile.

### 📚 Documentation
Please ensure any APIs you build are documented sufficiently, allowing any unfamiliar users to understand the endpoints and how to use them.

---

## 📨 Submission
If you are familiar with Git, then please regularly commit your work as you go along so that we can see how you work, including any tests you have built along with your submission. Once you're at a position to submit your work or reach the end of the allocated time limit, push up your Git repository with your code to either [Github](https://github.com), [Bitbucket](https://bitbucket.org) or similar and [send us an email](mailto:techchallenge@igloo.energy) with the link. We will then arrange a time for a call with you to discuss your solution.

**Note**: please also make sure to include a `README.md` file explaining how to get your application up and running, documenting any design choices you made and any extra information you'd like us to be aware of when reviewing your work.

## ❓Help
We'd advise that you spend no more than 3 hours of your time working on this. We're not looking for a fully fledged application but we want to get an idea of what you're capable of and how you work. 

If you get stuck, have any issues getting started or have any questions throughout the task, please [send us an email](mailto:techchallenge@igloo.energy) and we'll assist you however we can.

Good luck!

---

## ⚡️ Extensions
1. Outputting a split of how much of the user's salary is lost to tax at each band
2. Integrating the [National Insurance calculations](https://www.moneyadviceservice.org.uk/en/articles/tax-and-national-insurance-deductions#how-much-can-you-earn-before-you-need-to-pay-national-insurance) into your calculator, allowing a user to view how much National Insurance they have to pay based on their salary
3. Integrating [student loans](https://www.gov.uk/repaying-your-student-loan/what-you-pay) into your calculator, indicating how much a user will be paying towards their student loan each month.
